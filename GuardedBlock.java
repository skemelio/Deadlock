public class GuardedBlock{

  public static void main(String[] args) {

    final Box b = new Box("Box");

    Thread wrapper = new Thread(()->{
      b.wrap();
    });

    Thread unWrapper = new Thread(()->{
      b.unWrap();
    });

    wrapper.start();
    unWrapper.start();
  }

  public static class Box{
    private String label;
    private boolean isWrapped = true;

    public Box(String label){
        this.label = label;
    }

    public String getLabel(){
      return this.label;
    }

    public synchronized void unWrap(){
      while (!this.isWrapped) {
        try {
          System.out.println(this.getLabel()+" is unwrapped, waiting...");
          this.wait();
        } catch (InterruptedException e) {
          System.out.println(e.getMessage());
        }

        // it's wraped, unwrap it again
        System.out.println(this.getLabel()+" is wrapped, trying to unwrap it again...");
        this.sleep(2000);
        this.isWrapped = false;
        this.notifyAll();
      }
    }

    public synchronized void wrap(){
      while(this.isWrapped){
        try {
          // it's wrapped, wait
          System.out.println(this.getLabel()+" is wrapped, waiting...");
          this.wait();
        } catch (InterruptedException e) {
          System.out.println(e.getMessage());
        }
      }

      // it's unwapped, wrap it again
      System.out.println(this.getLabel()+" is unwrapped, trying to wrap it again...");
      this.sleep(4000);
      this.isWrapped = true;
      this.notifyAll();
    }

    private void sleep(long milli){
      try {
        Thread.sleep(milli);
      }catch (InterruptedException e) {
        System.out.println(e.getMessage());
      }
    }
  }

}
